# coding: utf-8
import sys,os
import readline
import logging
import atexit
import argparse
import json
from datetime import datetime
import urllib.parse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


from pip._vendor.pyparsing import empty

log = logging.getLogger( __name__ )
log.setLevel( logging.INFO )

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter( logging.Formatter( format ) )
log.addHandler( handler )

if __name__ == '__main__':
        # Parse arguments
    parser = argparse.ArgumentParser(description='DCS iovs analysis.',add_help=False)
    parser.add_argument("-f","--file", default="scthv.csv", help="input data file name.")
    parser.add_argument("-n","--node", default="/SCT/DCS/HV", help="input node name for selection.")
    parser.add_argument("-y","--yaxis", default=4.0, help="Limit of Y axis.")
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')

    args = parser.parse_args()
    nodename = args.node
    df=pd.read_csv(args.file)
    df['rdiff']=(df['rend']-df['rstart'])
    df['hdiff']=(df['holeend']-df['rend'])
    df['ripersec']=df['rcount']/df['rdiff']
    df['hipersec']=df['holecount']/df['hdiff']
    df['ratio']=df['ripersec']/df['hipersec']
    df_filtered=df.query('active>0 & state>0 & node==@nodename').copy()
    df_filtered['t0'] = pd.to_datetime(df_filtered['rstart'],unit='s')
    df_filtered.set_index('t0',inplace=True)
    # integrate on rolling window of 7 days
    u = df_filtered[['rcount','holecount','rdiff','hdiff']].rolling('7d').sum()
    u['r_2d'] = u['rcount'] / u['rdiff']
    u['h_2d'] = u['holecount'] / u['hdiff']
    df_filtered.join( u[['r_2d','h_2d']] )
    df_filtered = df_filtered.join( u[['r_2d','h_2d']] )
##    df_filtered[ df_filtered['rdiff']+df_filtered['hdiff'] < 3600*24*7 ].plot(y=['r_2d','h_2d'],linestyle='none',marker='.',rot = 45)

    outputfig = nodename
    fig = plt.figure(outputfig,figsize=[10,6])

    ax = df_filtered[ df_filtered['rdiff']+df_filtered['hdiff'] < 3600*24*7 ].plot(y=['r_2d','h_2d'],linestyle='none',marker='.',rot = 45)
    ymax = float(args.yaxis)
    ax.set_ylim(0,ymax)
    figfile = fig.canvas.get_window_title().replace('/','_') + '.png'
    plt.title(nodename)
    plt.xlabel('Time')
    plt.ylabel('Rate(iov/sec)')
    leg=plt.legend()
    leg.get_texts()[0].set_text('rate in runs (7d)')
    leg.get_texts()[1].set_text('rate between runs (7d)')
    plt.savefig(figfile)
#    fig.savefig( figfile )
