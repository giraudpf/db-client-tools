# coding: utf-8
import cx_Oracle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pandas.io.sql as psql

class DcsPandasTools(object):

    ora_conn = {
        'host': 'localhost',
        'port': 10121,
        'service': 'atlr.cern.ch',
        'schema': 'ATLAS_COND_TOOLS_R',
        'offset': 0,
        'limit': 10000
    }

    pag = """
    1540854000 ==> Tue Oct 30 00:00:00 CET 2018
    1541890800 ==> Sun Nov 11 00:00:00 CET 2018
    """
    mdt_qry = """
        select * from (
        select rownum rnum, IOV_SINCE/1000000 as sincet, IOV_UNTIL/1000000 as untilt, channel_id,
        "iMon_ML1", "v0set_ML1", "chanErrorFlag_ML1", "IOffset_ML1", "v1set_ML1", "IScale_ML1",
        "iMon_ML2", "v0set_ML2", "chanErrorFlag_ML2", "IOffset_ML2", "v1set_ML2", "IScale_ML2",
        "fsmCurrentState_ML1","fsmCurrentState_ML2"
        from ATLAS_COOLOFL_DCS.CONDBR2_F0055_IOVS where IOV_SINCE > %s and IOV_SINCE < %s
        and rownum <= %d
        order by IOV_SINCE asc
        ) where rnum > %d
    """
    mdt_state = {'STANDBY':2, 'RAMP_UP':10, 'ON': 99, 'RAMP_DOWN': 35, 'OFF': 0, 'UNKNOWN': -1, 'UNPLUGGED' : -9}
    
    ex = """
import pandas as pd
import pandas.io.sql as psql
chunk_size = 10000
offset = 0
dfs = []
while True:
  sql = "SELECT * FROM MyTable limit %d offset %d order by ID" % (chunk_size,offset)
  dfs.append(psql.read_frame(sql, cnxn))
  offset += chunk_size
  if len(dfs[-1]) < chunk_size:
    break
full_df = pd.concat(dfs)
    """

    def __init__(self, passwd=None, sincet=None, untilt=None, qry=None):  # noqa: E501
        self._passwd = passwd
        self._qry = qry
        if qry is None:
            self._qry = self.mdt_qry
        self._sincet = sincet
        self._untilt = untilt
        self._dsn = cx_Oracle.makedsn(host=self.ora_conn['host'],port=self.ora_conn['port'],service_name=self.ora_conn['service'])
        self._connection = cx_Oracle.connect(self.ora_conn['schema'], self._passwd, self._dsn )
        print('Initialised oracle connection')

    def readSql(self):
        qry = self._qry % (self._sincet,self._untilt)
        print('Initialised request to DB : %s ' % qry)
        self._df = pd.read_sql(qry,con=self._connection)
        self._df.loc[:,'times'] = pd.Series(pd.to_datetime(self._df['SINCET'], unit='ms').dt.floor('H'), index=self._df.index)
        return self._df

    def readChunkSql(self,limit,offset):
        qry = self._qry % (self._sincet,self._untilt,limit,offset)
        print('Initialised request to DB : %s ' % qry)
        sdf = pd.read_sql(qry,con=self._connection)
        sdf.loc[:,'times'] = pd.Series(pd.to_datetime(sdf['SINCET'], unit='ms').dt.floor('H'), index=sdf.index)
        return sdf

    def readSqlLarge(self,limit):
        dfs = []
        chksize = limit
        if chksize is None:
            chksize = self.ora_conn['limit']
        else:
            self.ora_conn['limit'] = limit
        offset = 0;
        while True:
            dfs.append(self.readChunkSql(chksize,offset))
            offset += self.ora_conn['limit']
            chksize = offset + self.ora_conn['limit']
            if len(dfs[-1]) < self.ora_conn['limit']:
                break
        self._df = pd.concat(dfs)
        self.write('/tmp/dcs.parquet','parquet')
        return self._df

    def read(self,filename,type):
        if 'parquet' in type:
            self._df = pd.read_parquet(filename)
        elif 'csv' in type:
            self._df = pd.read_csv(filename)
        elif 'sql' in type:
            self.readSql()
        else:
            self._df = pd.read_json(filename)
        return self._df

    def write(self, filename, type):
        if 'parquet' in type:
            self._df.to_parquet(filename)
        elif 'csv' in type:
            self._df.to_csv(filename)
        else:
            self._df.to_json(filename)

    def getmean(self):
        # return a dataframe
        return self._df.groupby(['CHANNEL_ID','times'],as_index=False).aggregate(np.mean)

    def gettimeserie(self,df,col,colcond,cval):
        #chdf = df[df[colcond] == cval]
        chdf = df[colcond]
        return pd.Series(chdf[col].values, index=chdf['times'])

    def tsplot(self,ts):
        ts.plot()
        plt.show()
