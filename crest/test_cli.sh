!#/bin/sh

python crestcli.py create --host localhost --type tags --tag MYTAG-02 --params timeType=time
python crestcli.py create --host localhost --type payloads --since 5100000 --tag MYTAG-02 --inpfile ./data/test-1.json
python crestcli.py create --host localhost --type payloads --since 11100000 --tag MYTAG-02 --inpfile ./data/test-2.json
python crestcli.py --host localhost ls --type iovs -t MYTAG-02
python crestcli.py --host localhost get --type payloads -p c788910d421d0aaa8ff135db4d1cb86ad1890d1135136a926aca1a8a3053eaaa