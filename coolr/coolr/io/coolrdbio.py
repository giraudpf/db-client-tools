"""
 HTTP client tool to exchange with COOL

 Retries requests with power law delays and a max tries limit

 @author: henri.louvin@cea.fr
"""

# Log
import logging
# JSON
import json
from datetime import datetime

from .httpio import HttpIo
log = logging.getLogger('coolrdb_client')


class CoolrDbIo(HttpIo):
    """
    (A)synchronous HTTP client
    """
    def __init__(self, server_url=None, max_tries=5, #pylint: disable=R0913
                 backoff_factor=1, asynchronous=False, loop=None):
        if server_url is None:
            server_url = 'http://atlasfrontier07.cern.ch:9090/coolrapi'
        super().__init__(server_url,
                         max_tries=max_tries,
                         backoff_factor=backoff_factor,
                         asynchronous=asynchronous,
                         loop=loop)
        self.headers = {"Content-Type" : "application/json", "Accept" : "application/json"}
        self.coolr_headers = {}
        self.cmddic = { 'nodes' : '/nodes', 'channels' : '/nodes/channels', \
            'tags' : '/tags', 'schemas' : '/schemas', 'iovs' : '/iovs', \
            'tail' : '/iovs/tail', 'ranges' : '/iovs/ranges', 'payloads' : '/payloads', \
            'globaltags' : '/globaltags', 'trace' : '/globaltagmaps', \
            'backtrace' : '/globaltagmaps/backtrace', 'run' : '/nemop/run'}

    def set_header(self, hdr):
        # example : {"X-Crest-PayloadFormat" : "JSON"}
        self.crest_headers = hdr

    def select(self, cmd='nodes', db=None, **kwargs):
        """
        request and export nodes data from the database in json format
        usage example: select(db='CONDBR2' , node='/MDT', schema='ATLAS_COOLOFL')
        """
        # define output fields
        valid_filters = ['schema', 'node', 'tag', 'since', 'until', 'chan', 'name', 'iovtype']
        log.debug(f'Using select arguments {cmd} {db} : {kwargs}')

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        criteria = {'db': db }
        for key, val in kwargs.items():
            criteria[key] = val
        try:
            if cmd == 'trace' or cmd == 'backtrace':
                gtagname = criteria['name']
                del criteria['name']
                self.cmddic[cmd] = self.cmddic[cmd]+'/'+gtagname
        except Exception as e:
            pass
        resp = self.get(self.cmddic[cmd], params=criteria)
        return resp.json()

    def search_runs(self, cmd='run', page=0, size=1000, sort='run:ASC',**kwargs):
        """
        request and export data from the database in json format
        usage example: search_runs(runmin='222222',runmax='333333')
        ?by=run>222222,run<333333”
        """
        # define output fields
        valid_filters = ['by']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        criteria = {'by': kwargs['by']}
        criteria['page'] = page
        criteria['size'] = size
        criteria['sort'] = sort

        # print(f'Sending nemop request: {self.cmddic[cmd]} using {criteria}')
        # send request
        resp = self.get(self.cmddic[cmd], params=criteria)
        return resp.json()
