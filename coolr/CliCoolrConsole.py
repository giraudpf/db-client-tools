'''
Created on Nov 24, 2017

@author: formica
'''

import cmd

import sys,os
import readline
import logging
import atexit
import argparse
import json
from datetime import datetime
from coolr.io import CoolrDbIo
from coolutils import *

from pip._vendor.pyparsing import empty

log = logging.getLogger( __name__ )
log.setLevel( logging.INFO )

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter( logging.Formatter( format ) )
log.addHandler( handler )

sys.path.append(os.path.join(sys.path[0],'..'))
historyFile = '.coolrconsole_hist'

class ConsoleUI(cmd.Cmd):
    """Simple command processor example."""
    cm = None
    prompt ='(Coolr): '
    homehist = os.getenv('CDMS_HISTORY_HOME', os.environ["HOME"])
    histfile = os.path.join( homehist, historyFile)
    host=None
    loc_parser=None
    import re
    rr = r"""
        ([<|>|:]+)  # match one of the symbols
    """
    rr = re.compile(rr, re.VERBOSE)

    def init_history(self, histfile):
        readline.parse_and_bind( "tab: complete" )
        readline.set_history_length( 100 )
        if hasattr( readline, "read_history_file" ):
            try:
                readline.read_history_file( histfile )
            except IOError:
                pass
            atexit.register( self.save_history, histfile )

    def save_history(self, histfile):
        log.info ('Saving history in %s' % histfile)
        readline.write_history_file( histfile )
    def set_host(self, url):
        self.host = url

    def get_args(self, line=None):
        argv=line.split()
        self.loc_parser = argparse.ArgumentParser(description="parser options for cli",add_help=False)
        group = self.loc_parser.add_mutually_exclusive_group()
        group.add_argument("-v", "--verbose", action="store_true")
        group.add_argument("-q", "--quiet", action="store_true")
        self.loc_parser.add_argument('cmd', nargs='?', default='schemas')
        self.loc_parser.add_argument('-h', '--help', action="store_true", help='show this help message')
        self.loc_parser.add_argument("-d", "--database", default='CONDBR2', help="the database: CONDBR2, COMP200, OFLP200")
        self.loc_parser.add_argument("-t", "--tag", help="the tag name")
        self.loc_parser.add_argument("-s", "--schema", help="the schema name")
        self.loc_parser.add_argument("-n", "--node", help="the node name")
        self.loc_parser.add_argument("-g", "--globaltag", help="the global tag name")
        self.loc_parser.add_argument("--iovtype", default='cooltime',help="the since time")
        self.loc_parser.add_argument("--since", help="the since time")
        self.loc_parser.add_argument("--until", default="INF", help="the until time")
        self.loc_parser.add_argument("--channel", default='%', help="the channel id or a name pattern")
        self.loc_parser.add_argument("--output", help="write json data into the provided output file")
        self.loc_parser.add_argument("-f", "--fields",  default='', help="the list of fields to show, separated with a comma")
        self.loc_parser.add_argument("-H", "--header", default="BLOB", help="set header request for payload: BLOB, JSON, ...")
        return self.loc_parser.parse_args(argv)

    def do_connect(self,url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = CoolrDbIo(server_url=url)
        log.info(f'Connected to {url}')

    def do_select(self, line):
        """select [schemas|tail|nodes|tags|channels|iovs|payloads] -s schema -n node -t sometag
        Select for data using the given options"""
        out = None
        cmd = None
        db = 'CONDBR2'
        cdic = {}
        fields = []
        selnode = {}
        iovbase = 'time'
        if line:
            log.info ("Searching iovs using %s " % line)
            args = self.get_args(line)
            cmd = args.cmd
            if args.help:
                self.loc_parser.print_help()
                return
            if args.database:
                db=args.database
            if args.schema:
                cdic['schema']=args.schema
            if args.node:
                cdic['node']=args.node
            if args.tag:
                cdic['tag']=args.tag
            if args.globaltag:
                cdic['name']=args.globaltag
            if cmd == 'backtrace':
                tagname = args.tag
                cdic['name'] = tagname
                cdic.pop('tag', None)
            if cmd == 'iovs' or cmd == 'payloads':
                if args.channel:
                    cdic['chan']=args.channel
                if args.since:
                    cdic['since']=args.since
                if args.until:
                    if args.until == 'INF':
                        args.until = 9223372036854776000
                    cdic['until']=args.until
                if args.iovtype:
                    cdic['iovtype'] = args.iovtype
                nodedic = { 'schema' : args.schema, 'node' : args.node, 'db' : args.database}
                nodelist = self.cm.select(cmd='nodes',db=db,**cdic)
                selnode = nodelist[0]
                iovbase = selnode['nodeIovBase']
            if args.fields:
                if args.fields == 'help':
                    if cmd == 'nodes':
                        print(f'Fields for {cmd} are {nodesfieldsdic.keys()}')
                    if cmd == 'tags':
                        print(f'Fields for {cmd} are {tagsfieldsdic.keys()}')
                    if cmd == 'iovs' or cmd == 'tail':
                        print(f'Fields for {cmd} are {iovsfieldsdic.keys()}')
                    return
                fields = args.fields.split(',')
            if cmd == 'payloads':
                cdic['qrytype'] = 'none'
            log.info(f'Retrieve data using {cmd} and dictionary {cdic}')
            out = self.cm.select(cmd=cmd,db=db,**cdic)

        else:
            log.info ('Cannot search ... arguments are probably missing, type -h for help')
        if args.output:
            fout = args.output
            print(f'Dump json data to output file {fout}')
            with open(fout, 'w') as local_file:
                json.dump(out, local_file)
        else:
            coolr_print(out,format=fields, cmd=cmd, iovbase=iovbase)

    def do_convert(self, line):
        """convert date
        Convert a date to UTC unix time."""
        dt=datetime.fromisoformat(line)
        log.info('create time from string %s %s' % (line,dt.timestamp()))
        since=int(dt.timestamp()* 1000)
        print(f'date {line} = {since}')

    def do_exit(self, line):
        return True
    def do_quit(self, line):
        return True
    def emptyline(self):
        pass

    def preloop(self):
        self.init_history(self.histfile)

    def postloop(self):
        print

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks # you need to install pysocks (use the command: pip install pysocks)
# Configuration

# Remove this if you don't plan to "deactivate" the proxy later
#        default_socket = socket.socket
# Set up a proxy
#            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print ('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST,SOCKS5_PROXY_PORT))
        except:
            print ('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST,SOCKS5_PROXY_PORT))


if __name__ == '__main__':
        # Parse arguments
    parser = argparse.ArgumentParser(description='Coolr browser.')
    parser.add_argument('--host', default='atlasfrontier07.cern.ch',
                        help='Host of the COOLR service (default: atlasfrontier07.cern.ch)')
    parser.add_argument('--api', default='coolrapi',
                        help='Base name of the api (default: coolrapi)')
    parser.add_argument('--port', default='8000',
                        help='Port of the COOLR service (default: 8000)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    args = parser.parse_args()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot,args.host,args.port,args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST']=host
    ui = ConsoleUI()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129 ; if you want another address please set CDMS_SOCKS_HOST and _PORT env vars")
        ui.socks()

    ui.cmdloop()
