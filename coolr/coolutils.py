'''
Created on Nov 24, 2017

@author: formica
'''

import sys, os
import logging
import atexit
from datetime import datetime
import hashlib
import json
import pytz

runmask = 0xffffffff00000000
lumimask = 0x00000000ffffffff

schemasfieldsdic = {
    'schemaname': '{schemaname:25.25s}',
    'numfolders': '{numfolders:12d}',
}
schemasfieldsdicheader = {
    'schemaname': {'key': '{schemaname:25.25s}', 'val': 'Schema'},
    'numfolders': {'key': '{numfolders:12s}', 'val': 'N Folders'},
}
rangesfieldsdic = {
    'since': '{since:25d}',
    'until': '{until:25d}',
}
rangesfieldsdicheader = {
    'since': {'key': '{since:25s}', 'val': 'Since'},
    'until': {'key': '{until:25s}', 'val': 'Until'},
}
runsfieldsdicheader = {
    'run': {'key': '{run:10s}', 'val': 'Run'},
    'ctime': {'key': '{ctime:15s}', 'val': 'ctime'},
    'utime': {'key': '{utime:15s}', 'val': 'utime'},
    'active': {'key': '{active:5s}', 'val': 'Active'},
    'state': {'key': '{state:5s}', 'val': 'State'},
    'runstart': {'key': '{runstart:15s}', 'val': 'Start'},
    'runend': {'key': '{runend:15s}', 'val': 'End'},
    'runtype': {'key': '{runtype:15s}', 'val': 'Type'},
    'nlb': {'key': '{nlb:10s}', 'val': 'NLB'},
}
runsfieldsdic = {
    'run': '{run:10d}',
    'ctime': '{ctime:15d}',
    'utime': '{utime:15d}',
    'active': '{active:5d}',
    'state': '{state:5d}',
    'runstart': '{runstart:15d}',
    'runend': '{runend:15d}',
    'runtype': '{runtype:15s}',
    'nlb': '{nlb:10d}',
}
nodesfieldsdic = {
    'schemaName': '{schemaName:25.25s}',
    'nodeFullpath': '{nodeFullpath:50s}',
    'nodeId': '{nodeId:5d}',
    'nodeDescription': '{nodeDescription:50s}',
    'folderPayloadSpec': '{folderPayloadSpec:50s}',
    'nodeIsleaf': '{nodeIsleaf:5d}',
    'nodeInstime': '{nodeInstime:30s}'
}
nodesfieldsdicheader = {
    'schemaName': {'key': '{schemaName:25.25s}', 'val': 'Schema'},
    'nodeFullpath': {'key': '{nodeFullpath:50s}', 'val': 'Node'},
    'nodeId': {'key': '{nodeId:5s}', 'val': 'Id'},
    'nodeDescription': {'key': '{nodeDescription:50s}', 'val': 'Description'},
    'folderPayloadSpec': {'key': '{folderPayloadSpec:50s}', 'val': 'Specifications'},
    'nodeIsleaf': {'key': '{nodeIsleaf:5s}', 'val': 'Leaf'},
    'nodeInstime': {'key': '{nodeInstime:30s}', 'val': 'Insertion Time'}
}
tagsfieldsdic = {
    'schemaName': '{schemaName:25.25s}',
    'nodeFullpath': '{nodeFullpath:50s}',
    'tagId': '{tagId:5d}',
    'tagName': '{tagName:50s}',
    'tagLockStatus': '{tagLockStatus:5d}',
    'sysInstime': '{sysInstime:30s}'
}
tagsfieldsdicheader = {
    'schemaName': {'key': '{schemaName:25.25s}', 'val': 'Schema'},
    'nodeFullpath': {'key': '{nodeFullpath:50s}', 'val': 'Node'},
    'tagId': {'key': '{tagId:5s}', 'val': 'Id'},
    'tagName': {'key': '{tagName:50s}', 'val': 'Tag'},
    'tagLockStatus': {'key': '{tagLockStatus:5s}', 'val': 'Lock'},
    'sysInstime': {'key': '{sysInstime:30s}', 'val': 'Insertion Time'}
}
gltagsfieldsdic = {
    'gtagName': '{gtagName:25.25s}',
    'gtagLockStatus': '{gtagLockStatus:5d}',
    'nschemas': '{nschemas:10d}',
    'sysInstime': '{sysInstime:30s}',
    'gtagDescription': '{gtagDescription:60s}',
}
gltagsfieldsdicheader = {
    'gtagName': {'key': '{gtagName:25.25s}', 'val': 'GlobalTag'},
    'gtagLockStatus': {'key': '{gtagLockStatus:5s}', 'val': 'Lock'},
    'nschemas': {'key': '{nschemas:10s}', 'val': 'N schemas'},
    'sysInstime': {'key': '{sysInstime:30s}', 'val': 'Insertion Time'},
    'gtagDescription': {'key': '{gtagDescription:60s}', 'val': 'Description'},
}
iovsfieldsdic = {
    'objectid': '{objectid:10d}',
    'iovSince': '{iovSince:20d}',
    'iovUntil': '{iovUntil:20d}',
    'sincestr': '[{sincestr:18s}]',
    'untilstr': '[{untilstr:18s}]',
    'channelId': '{channelId:15d}',
    'channelName': '{channelName:20s}',
    'userTagId': '{userTagId:6d}',
    'sysInstime': '{sysInstime:25s}'
}
iovsfieldsdicheader = {
    'objectid': {'key': '{objectid:10s}', 'val': 'Obj-ID'},
    'iovSince': {'key': '{iovSince:20s}', 'val': 'Since'},
    'iovUntil': {'key': '{iovUntil:20s}', 'val': 'Until'},
    'sincestr': {'key': '{sincestr:18s}', 'val': 'SinceStr'},
    'untilstr': {'key': '{untilstr:18s}', 'val': 'UntilStr'},
    'channelId': {'key': '{channelId:15s}', 'val': 'Chan-ID'},
    'channelName': {'key': '{channelName:20s}', 'val': 'Chan-Name'},
    'userTagId': {'key': '{userTagId:6s}', 'val': 'Tag-ID'},
    'sysInstime': {'key': '{sysInstime:25s}', 'val': 'Insertion Time'}
}
pyldiovsfieldsdic = {
    'OBJECT_ID': '{OBJECT_ID:10d}',
    'IOV_SINCE': '{IOV_SINCE:20d}',
    'IOV_UNTIL': '{IOV_UNTIL:20d}',
    'sincestr': '[{sincestr:18s}]',
    'untilstr': '[{untilstr:18s}]',
    'CHANNEL_ID': '{CHANNEL_ID:15d}',
    'USER_TAG_ID': '{USER_TAG_ID:6d}',
    'SYS_INSTIME': '{SYS_INSTIME:25s}'
}
pyldiovsfieldsdicheader = {
    'OBJECT_ID': {'key': '{OBJECT_ID:10s}', 'val': 'Obj-ID'},
    'IOV_SINCE': {'key': '{IOV_SINCE:20s}', 'val': 'Since'},
    'IOV_UNTIL': {'key': '{IOV_UNTIL:20s}', 'val': 'Until'},
    'sincestr': {'key': '{sincestr:18s}', 'val': 'SinceStr'},
    'untilstr': {'key': '{untilstr:18s}', 'val': 'UntilStr'},
    'CHANNEL_ID': {'key': '{CHANNEL_ID:15s}', 'val': 'Chan-ID'},
    'USER_TAG_ID': {'key': '{USER_TAG_ID:6s}', 'val': 'Tag-ID'},
    'SYS_INSTIME': {'key': '{SYS_INSTIME:25s}', 'val': 'Insertion Time'}
}


class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def coolr_print(cdata, format=[], cmd=None, iovbase='cool', output=None):
    print(f'Retrieved a list of {len(cdata)} entries using command {cmd}')
    if output:
        print(f'Dump json data to output file {output}')
        with open(output, 'w') as local_file:
            json.dump(cdata, local_file)
    print(f'Print formatted output for command {cmd}')
    if (cmd == 'schemas'):
        dprint(format, schemasfieldsdicheader, schemasfieldsdic, cdata, iovbase)
    elif (cmd == 'ranges'):
        dprint(format, rangesfieldsdicheader, rangesfieldsdic, cdata, iovbase)
    elif (cmd == 'trace' or cmd == 'backtrace'):
        if len(format) == 0:
            format = ['schemaName', 'nodeFullpath', 'gtagName', 'tagName', 'tagId', 'tagLockStatus']
        headerfmtstr = '{schemaName:25.25s} {nodeFullpath:50s} {gtagName:30s} {tagName:30s} {tagId:5s} {tagLockStatus:5s}'
        print(
            headerfmtstr.format(schemaName='Schema', nodeFullpath='Node', gtagName='GlobalTag Name', tagName='Tag Name',
                                tagId='TagID', tagLockStatus='Lock'))
        fmtstr = '{schemaName:25.25s} {nodeFullpath:50s} {gtagName:30s} {tagName:30s} {tagId:5d} {tagLockStatus:5d}'
        for xt in cdata:
            print(fmtstr.format(schemaName=xt['schemaName'], nodeFullpath=xt['nodeFullpath'], gtagName=xt['gtagName'],
                                tagName=xt['tagName'], tagId=xt['tagId'], tagLockStatus=xt['tagLockStatus']))
    elif (cmd == 'nodes'):
        dprint(format, nodesfieldsdicheader, nodesfieldsdic, cdata, iovbase)
    elif (cmd == 'tags'):
        dprint(format, tagsfieldsdicheader, tagsfieldsdic, cdata, iovbase)
    elif (cmd == 'globaltags'):
        dprint(format, gltagsfieldsdicheader, gltagsfieldsdic, cdata, iovbase)
    elif (cmd == 'iovs' or cmd == 'tail'):
        dprint(format, iovsfieldsdicheader, iovsfieldsdic, cdata, iovbase)
    elif (cmd == 'run'):
        dprint(format, runsfieldsdicheader, runsfieldsdic, cdata, iovbase)
    else:
        print(cdata)


def coolr_coolcopy_print(cdata, dbname='CONDBR2', cmd=None, iovbase='cool', output=None):
    print(f'Retrieved a list of {len(cdata)} entries using command {cmd}')
    if output:
        print(f'Dump json data to output file {output}')
        with open(output, 'w') as local_file:
            json.dump(cdata, local_file)
    print(f'Print formatted output for command {cmd}')
    if (cmd == 'trace'):
        cd = {}
        sd = {}
        cd['oraclesource'] = 'oracle://ATLAS_COOLPROD'
        cd['oraclereader'] = 'user=ATLAS_COOL_READER;password=${READER_PASS}'
        sd['sqlitedest'] = 'sqlite://'
        ccoptions = '-create -cti -hi -readoracle '
        if output:
            print(f'Dump COOL copy commands to output file {output}')
            with open(output, 'w') as local_file:
                for xt in cdata:
                    cd['schema'] = 'schema=%s' % xt['schemaName']
                    cd['dbname'] = 'dbname=%s' % dbname
                    sd['filename'] = 'schema=./%s.sqlite' % xt['schemaName']
                    sd['dbname'] = 'dbname=%s' % dbname
                    msg = 'AtlCoolCopy.exe \"%s;%s;%s,%s\" \"%s;%s;%s\" %s -f %s -t %s' % (cd['oraclesource'],
                                                                                             cd['schema'],
                                                                                             cd['dbname'],
                                                                                             cd['oraclereader'],
                                                                                             sd['sqlitedest'],
                                                                                             sd['filename'],
                                                                                             sd['dbname'],
                                                                                             ccoptions,
                                                                                             xt['nodeFullpath'],
                                                                                             xt['tagName']
                                                                                             )
                    local_file.write(f'{msg}\n')
    else:
        print(f'Cannot dump coolcopy commands for {cmd}')


def coolr_payload_print(cdata, format=[], cmd=None, iovbase='cool', output=None, dumpfield=None):
    print(f'Retrieved a list of {len(cdata)} payload entries using command {cmd}')
    if (cmd == 'payloads'):
        dprintpyld(format, pyldiovsfieldsdicheader, pyldiovsfieldsdic, cdata, iovbase, dumpfield)


def dprint(format, headerdic, datadic, cdata, iovbase):
    if len(format) == 0:
        format = datadic.keys()
    headerfmtstr = ' '.join([headerdic[k]['key'] for k in format])
    headic = {}
    for k in format:
        headic[k] = headerdic[k]['val']
    print(color.GREEN + headerfmtstr.format(**headic) + color.END)
    # print('Use format %s' % format)
    fmtstr = ' '.join([datadic[k] for k in format])
    # print('Format string %s'%fmtstr)
    for xt in cdata:
        adic = {}
        for k in format:
            if 'sincestr' == k:
                xt['sincestr'] = convert_to_type(xt['iovSince'], iovbase)
            if 'untilstr' == k:
                xt['untilstr'] = convert_to_type(xt['iovUntil'], iovbase)
            if xt[k] is None:
                xt[k] = ' - '
            adic[k] = xt[k]
        # print('Use dictionary %s'%adic)
        print(fmtstr.format(**adic))


def dprintpyld(format, headerdic, datadic, cdata, iovbase, dumpfield):
    if len(format) == 0:
        format = datadic.keys()
    headerfmtstr = ' '.join([headerdic[k]['key'] for k in format])
    headic = {}
    for k in format:
        headic[k] = headerdic[k]['val']
    print(headerfmtstr.format(**headic))
    # print('Use format %s' % format)
    fmtstr = ' '.join([datadic[k] for k in format])
    # print('Format string %s'%fmtstr)
    dataarr = cdata['data_array']
    for xt in dataarr:
        # print('Try to print %s' % xt)
        adic = {}
        for k in format:
            if 'sincestr' == k:
                xt['sincestr'] = convert_to_type(xt['IOV_SINCE'], iovbase)
            if 'untilstr' == k:
                xt['untilstr'] = convert_to_type(xt['IOV_UNTIL'], iovbase)
            if xt[k] is None:
                xt[k] = ' - '
            adic[k] = xt[k]
        # print('Use dictionary %s'%adic)
        pylddic = {}
        for keys in xt:
            if keys not in format and keys != 'NEW_HEAD_ID' \
                    and keys != 'PAYLOAD_ID':
                pylddic[keys] = xt[keys]
                # print(f'Fill payload dic for {keys} = {xt[keys]}')
        data_str = json.dumps(pylddic, sort_keys=True)
        #print('Dumping dictionary: %s' % data_str)
        data_md5 = hashlib.md5(data_str.encode()).hexdigest()
        print(fmtstr.format(**adic), f'{data_md5}')
        if dumpfield is not None and dumpfield != 'none':
            with open(dumpfield+'_'+str(xt['IOV_SINCE'])+'_'+str(xt['IOV_UNTIL'])+'.txt', 'w') as f:
                print(pylddic[dumpfield], file=f)

def convert_to_type(atime, iovbase):
    if iovbase == 'run-lumi':
        if atime == 9223372036854775807:
            return 'MAX-RUN'
        arun = (atime & runmask) >> 32
        alumi = int(atime & lumimask)
        return f'{arun}-{alumi}'
    if iovbase == 'time':
        mst = int(atime / 1000000000)
        dt = datetime.fromtimestamp(mst, tz=pytz.utc)
        return dt.isoformat()
    return atime
