#!/usr/bin/env python

'''
Created on Nov 24, 2019

@author: formica
'''

import sys,os
import readline
import logging
import atexit
import argparse
import json
from datetime import datetime
from coolr.io import CoolrDbIo
from coolutils import *
from coolrcli import CoolR

from pip._vendor.pyparsing import empty

log = logging.getLogger( __name__ )
log.setLevel( logging.INFO )

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter( logging.Formatter( format ) )
log.addHandler( handler )

sys.path.append(os.path.join(sys.path[0],'..'))

if __name__ == '__main__':
        # Parse arguments
    parser = argparse.ArgumentParser(description='Get DCS data for a given folder using only specific run selection.',add_help=False)
    parser.add_argument('--cmd', choices=['dcs'], default='schemas')
    parser.add_argument('--host', default='atlasfrontier07.cern.ch',
                        help='Host of the COOLR service (default: atlasfrontier07.cern.ch)')
    parser.add_argument('--api', default='coolrapi',
                        help='Base name of the api (default: coolrapi)')
    parser.add_argument('--port', default='8000',
                        help='Port of the COOLR service (default: 8000)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    parser.add_argument('--payloadfmt', action='store_true',
                        help='Dump payload as dictionary (default: false)')
    parser.add_argument("-d", "--database", default='CONDBR2', help="the database: CONDBR2, COMP200, OFLP200")
    parser.add_argument("-s", "--schema", default="ATLAS_COOLOFL_DCS", help="the schema name.")
    parser.add_argument("-n", "--node", default="/SCT/DCS/HV", help="the node name")
    parser.add_argument("--runshift", default=0, help="the number of seconds to delay the run start time by")
    parser.add_argument("--runmin", default=364000, help="the min run number")
    parser.add_argument("--runmax", default=370000, help="the max run number")
    parser.add_argument("--channel", default='%', help="the channel id or a name pattern")
    parser.add_argument("--output", help="write json data into the provided output file")

    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    args = parser.parse_args()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot,args.host,args.port,args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST']=host
    ui = CoolR()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    schemaname = ''
    nodename = ''
    if args.schema:
        schemaname = args.schema
    if args.node:
        nodename = args.node
    rundic = { 'cmd' : 'run', 'runmin' : args.runmin, 'runmax' : args.runmax, 'output' : None, 'page' : 0, 'size' : 7000, 'sort' : 'run:ASC'}
    runlist = ui.do_nemop(rundic)
    print(f'Found run list of size {len(runlist)}')
    prevquerydic = {}
    prevmsg = ''
    doskip=False
    for run in runlist:
        log.info(f'Analyze run {run}')
        rs = int(run['runstart'])+int(args.runshift)
        re = int(run['runend'])
        rn = run['run']
        if rs == 0 or re == 0:
            doskip=True
            continue
        if 'rn' in prevquerydic:
            if doskip:
                doskip=False
                continue
            hole_rs = prevquerydic['re']
            hole_re = rs
            holedic = { 'skip': True, 'cmd' : 'iovs', 'schema' : schemaname, 'node': nodename,'since' : f'{hole_rs}000000000', 'until' : f'{hole_re}000000000', 'output' : None}
            holeiovslist=ui.do_select(holedic)
            prevcount = prevquerydic['rcount']
            prevrn = prevquerydic['rn']
            prevrs = prevquerydic['rs']
            prevre = prevquerydic['re']
            ract = prevquerydic['active']
            rstate = prevquerydic['state']
            rtype = prevquerydic['runtype']
            rnlb = prevquerydic['nlb']
            msg = f'RUN+HOLE: {nodename}, {prevrn}, {prevrs}, {prevre}, {prevcount}, {hole_re}, {len(holeiovslist)}, {ract}, {rstate}, {rtype}, {rnlb}'
            log.info(msg)

        runquerydic = { 'skip': True, 'cmd' : 'iovs', 'schema' : schemaname, 'node': nodename,'since' : f'{rs}000000000', 'until' : f'{re}000000000', 'output' : None}
        iovslist=ui.do_select(runquerydic)
        prevquerydic = { 'rs' : rs, 're' : re, 'rn' : rn, 'rcount' : len(iovslist), 'active' : run['active'], 'state': run['state'], 'runtype' : run['runtype'], 'nlb' : run['nlb']}
        msg = f'RUN: {rn}, {rs}, {re}, {len(iovslist)}'
        log.info(msg)
