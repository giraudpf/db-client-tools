# COOLR client

## Requirements
Need python >=3.5

## Installation
Can be installed via pip. Once you have activated from the previous instructions the virtual environment you can:
`pip install . `
or
`pip install . --user`
for local user installation.

## coolr tool (coolrcli.py)
Simple command line tool to browse COOL data using options. It is for sure the most efficient way for scripting.
Launch the command line after the library has been installed.
### List schemas
```
python coolrcli.py --cmd schemas -s COOLONL
```
### List nodes in schemas
```
python coolrcli.py --cmd nodes -s ATLAS_COOLOFL_TILE
```
### List tags in schema and node
You can either just put a schema or also some pattern for node (COOL folder) name.
```
python coolrcli.py --cmd nodes -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB
```
### List iovs in schema, node (and tag for multi version)
You should put a schema, a node and eventually a tag (mandatory for multi version).
 * tail : dump last 1000 iovs
```
python coolrcli.py --cmd tail -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-21
```
 * iovs : dump an iovs selection using time range
```
python coolrcli.py --cmd iovs -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-21 --since 364292-0 --until 364295-0 --iovtype run-lumi
```
or
```
python coolrcli.py --cmd iovs -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-21 --since 1561246381899776 --until 1575905105281024
```

### List payloads in schema, node (and tag for multi version)
You should put a schema, a node and eventually a tag (mandatory for multi version).
 * payloads : dump as a json
```
python coolrcli.py --cmd payloads -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-21 --since 364292-0 --until 364295-0 --iovtype run-lumi
```
 * payloads : dump in an external file
```
python coolrcli.py --cmd payloads -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-21 --since 364292-0 --until 364295-0 --iovtype run-lumi --output /tmp/mdtrt.json
```
 * payloads : dump only md5 checksum
```
python coolrcli.py --cmd payloads -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-21 --since 364292-0 --until 364295-0 --iovtype run-lumi --payloadfmt
```
### List globaltags
```
python coolrcli.py --cmd globaltags
```
or with pattern for filtering
```
python coolrcli.py --cmd globaltags -g BLKP
```
### List mappings
 * trace: find the tags associated to a global tag
```
python coolrcli.py --cmd trace -g CONDBR2-BLKPA-2018-15
```
 * backtrace: find the globaltags associated with a tag
```
python coolrcli.py --cmd backtrace -t CscT0base-RUN2-BLK-UPD1-001-00
```
### List runs
 * run: retrieve run information from NEMOP table
```
python coolrcli.py --cmd run --runmin 360000 --runmax 370000
```

## coolr console (CliCoolrConsole.py)
This is a prototype inspired to AtlCoolConsole.
Launch the command line after the library has been installed.
```
python CliCoolrConsole.py
```

You have now a console connected to the default COOLR server (atlasfrontier07.cern.ch:8000).
Browse COOL data using the commands available. Type `help <command>` for help, and `command -h` for more detailed options.
Here below we provide a terminal output:
```
(py36-venv) [lxplus775] hackathon/db-client-tools/coolr % python CliCoolrConsole.py -h
usage: CliCoolrConsole.py [-h] [--host HOST] [--api API] [--port PORT]
                          [--socks] [--ssl]

Coolr browser.

optional arguments:
  -h, --help   show this help message and exit
  --host HOST  Host of the COOLR service (default: atlasfrontier07.cern.ch)
  --api API    Base name of the api (default: coolrapi)
  --port PORT  Port of the COOLR service (default: 8000)
  --socks      Activate socks (default: false)
  --ssl        Activate ssl (default: false)
(py36-venv) [lxplus775] hackathon/db-client-tools/coolr % python CliCoolrConsole.py
INFO:__main__: The host is set to http://atlasfrontier07.cern.ch:8000/coolrapi
INFO:__main__: Connected to http://atlasfrontier07.cern.ch:8000/coolrapi
INFO:__main__: Start application
(Coolr): help

Documented commands (type help <topic>):
========================================
connect  convert  help  select

Undocumented commands:
======================
exit  quit

(Coolr): help select
select [schemas|nodes|tags|channels] -s schema -n node -t sometag
        Select for data using the given options
```
Now some more specific example.

### schemas
`select schemas` or `select schemas -d OFLP200` if you want to change the default database (CONDBR2).

### nodes
* `select nodes -s ATLAS_COOLOFL`
or
* `select nodes -d OFLP200 -s ATLAS_COOLOFL_MDT`

### tags
* `select tags -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t UPD`
Filter only UPD tags from folder `/MDT/RTBLOB`.

### iovs, tail, ranges
 * `select tail -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-18` Get the last 1000 iovs.
 * `select iovs -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-18 --until 1349100767281152 --since 1348950443425792 --channel 6307840`
Get iovs in a range.
 * `select ranges -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-18`
Get all ranges.

### payloads
`select payloads -s ATLAS_COOLOFL_MDT -n /MDT/RTBLOB -t MDTRT-RUN2-UPD4-18 --until 1349100767281152 --since 1348950443425792 --channel 6307840`

## Options
When using the selec command above, you can specify the wanted output fields or if you want to dump the JSON in an output file.
* `select <command> -f help`, to know which fields can be printed
* `select <command> .... --output /tmp/something.json`, to dump the output in json
