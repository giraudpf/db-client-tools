'''
Created on Nov 24, 2017

@author: formica
'''

import argparse
from datetime import datetime

from alignmon.io import AlignDbIo
from alignmon.utils import *

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

sys.path.append(os.path.join(sys.path[0], '..'))


class AlignCli(object):
    """Simple command processor example."""

    def __init__(self, server_url=None):
        if server_url is None:
            server_url = 'http://localhost:8080/api'

        cm = None
        self.host = server_url
        loc_parser = None
        import re
        rr = r"""
            ([<|>|:]+)  # match one of the symbols
        """
        rr = re.compile(rr, re.VERBOSE)

    def set_host(self, url):
        self.host = url

    def set_parser(self, prs):
        self.loc_parser = prs

    def do_connect(self, url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = AlignDbIo(server_url=url, max_tries=1)
        log.info(f'Connected to {url}')

    def do_dump(self, args=None):
        """dump <datatype> [-i iovid]
        Dump data for a given iovid: iovs.
        datatype: iovs.
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = 'iovs'
        tagname = None
        cooltagname = None
        optline = '%'
        iovid = None
        cdic = {}
        if args:
            print(f'Received args {args}')
            if 'type' in args:
                cmd = args['type']
            log.info(f'Searching for data of type {cmd}')
            if 'iovid' in args:
                iovid = args['iovid']
        if cmd == 'iovs':
            if iovid is None:
                log.error('Cannot dump iov {iovid}')
                return
            log.info(f'Get iov using {iovid}')
            out = self.cm.get_iov_file(iovid=iovid, format='COOL')
            print(f'Found file {out}')
            return out
        return None

    def do_ls(self, args=None):
        """ls <datatype> [-t tag_name] [--olname opticalline_name] [other options: --size, --page, --sort, --format]
        Search for data collection of different kinds: opticallines, tags.
        datatype: opticallines, iovs, tags.
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = 'tags'
        tagname = None
        cooltagname = None
        optline = '%'
        iovid = None
        cdic = {}
        if args:
            print(f'Received args {args}')
            if 'type' in args:
                cmd = args['type']
            log.info(f'Searching for data of type {cmd}')
            if 'tag' in args:
                tagname = args['tag']
            if 'cooltag' in args:
                cooltagname = args['cooltag']
            if 'olname' in args:
                optline = args['olname']
            if 'iovid' in args:
                iovid = args['iovid']
# Init general sort, since, until only if they are present
            sort = args['sort']
            if 'since' in args and args['since'] is not None:
                cdic['since'] = args['since']
            if 'until' in args and args['until'] is not None:
                cdic['until'] = args['until']

            if 'cut' in args and args['cut'] is not None:
                cutstringarr = args['cut'].split(',')
                for el in cutstringarr:
                    ss = self.rr.findall(el)
                    ##print(el,ss)
                    (k, v) = el.split(ss[0])
                    cdic[k] = f'{ss[0]}{v}'
                log.info('use cut params : %s' % cdic)

            fields = []
            if 'fields' in args:
                log.debug('fields is %s' % args.get('fields'))
                if 'none' == args['fields']:
                    fields = []
                else:
                    fields = args.get('fields').split(',')

            # Check the data type and search
            if cmd == 'tags':
                if tagname is None:
                    tagname = "%"
                cdic['name'] = tagname
                if "none" == sort:
                    sort = "iovTag:ASC"
                log.info(f'Search tags using {tagname}')
                out = self.cm.search_tags(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AligntagSetDto'
            elif cmd == 'cooltags':
                log.info(f'Search cool tags  using {cooltagname}')
                if cooltagname is None:
                    cooltagname = "%"
                cdic['name'] = cooltagname
                if "none" == sort:
                    sort = "pk.coolTag:ASC"
                out = self.cm.search_cooltags(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AlignCoolTagSetDto'
            elif cmd == 'iovs':
                log.info(f'Search iovs using {tagname}')
                if tagname is None:
                    log.error('Cannot search iovs without a tagname specified')
                    return
                if "none" == sort:
                    sort = "iovId:ASC"
                out = self.cm.search_iovs(tagname=tagname, page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AligniovSetDto'
            elif cmd == 'corrections':
                log.info(f'Search corrections using {tagname}')
                if tagname is None:
                    log.error('Cannot search corrections without a tagname specified')
                    return
                if "none" == sort:
                    sort = "hwElement:ASC"
                cdic['iovId'] = iovid
                cdic['tagname'] = tagname
                out = self.cm.search_corrections(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AligncorrectionsSetDto'
            elif cmd == 'fitsteps':
                log.info(f'Search fitsteps')
                if "none" == sort:
                    sort = "fitId:ASC"
                cdic['iovId'] = iovid
                cdic['tagname'] = tagname
                out = self.cm.search_fitsteps(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AlignFitStepsSetDto'
            elif cmd == 'fitpulls':
                log.info(f'Search fitpulls using {tagname}')
                if tagname is None:
                    log.error('Cannot search fitsteps without a tagname specified')
                    return
                if "none" == sort:
                    sort = "pk.olname:ASC"
                cdic['iovId'] = iovid
                cdic['tagname'] = tagname
                out = self.cm.search_fitpulls(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AlignFitPullsSetDto'
            elif cmd == 'opticallines':
                cdic['olname'] = optline
                log.info(f'Search opticallines using {optline}')
                out = self.cm.search_opticallines(**cdic)
                out['format'] = 'OpticallinesSetDto'
            elif cmd == 'imtags':
                log.info(f'Search intervalmaker tags using {tagname}')
                out = self.cm.search_im_tags(tagname=tagname)
                out['format'] = 'IntervalMakerTagSetDto'
            elif cmd == 'imconfigs':
                log.info(f'Search intervalmaker configurations using {tagname}')
                out = self.cm.search_im_configurations(tagname=tagname)
                out['format'] = 'IntervalMakerConfigurationSetDto'
            elif cmd == 'imiovs':
                log.info(f'Search intervalmaker iovs using {tagname}')
                out = self.cm.search_im_iovs(mode=args['mode'], tagname=tagname, since=cdic['since'], until=cdic['until'], fmt='iso')
                out['format'] = 'IntervalMakerIovSetDto'
            else:
                print(f'Command {cmd} is not recognized in this context')
        server_print(out, format=fields)
        return out

    def do_info(self, args):
        """info <datatype>
         Provide help for k=val pairs to be used in params for the requested datatype
         """
        if args:
            cmd = None
            if 'type' in args:
                cmd = args['type']
            if cmd == 'tags':
                print(f'Fields for {cmd} are {tagfieldsdictypes.keys()}')
            elif cmd == 'opticallines':
                print(f'Fields for {cmd} are {optlinefieldsdictypes.keys()}')
            else:
                print('not available for this command')

    def do_create(self, args):
        """create <datatype> k=v,k1=v1,....
        Create a new tag or cool tag, using a series of k=val pairs, separated
        by commas
        """
        out = None
        cmd = None
        pdic = {}
        tname = None
        cooltname = None
        optline = None
        iovid = None
        if args:
            if 'type' in args:
                cmd = args['type']
            log.info(f'Creating object for type {cmd}')
            if 'tag' in args and cmd in ['tags', 'iovs', 'cooltags', 'imconfigs']:
                tname = args['tag']
            if 'cooltag' in args and cmd in ['cooltags', 'cooliovs']:
                cooltname = args['cooltag']
            if 'iovid' in args and cmd in ['iovs', 'corrections']:
                iovid = args['iovid']
            if 'olname' in args and cmd in ['opticallines']:
                optline = args['olname']

            # Check if the user needs hints on fields
            if 'params' in args and args['params'] is not None:
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
            # Check if the user needs hints on fields
            corrdic = {}
            if 'corrections' in args and args['corrections'] is not None:
                corrarr = args['corrections'].split(',')
                for par in corrarr:
                    kv = par.split('=')
                    corrdic[kv[0]] = kv[1]

            if cmd == 'tags':
                log.info(f'Creating tag {tname} and args {pdic}')
                if tname is None:
                    print(f"Cannot create tag without the name provided via -t or --tag")
                    return
                out = self.cm.create_tags(name=tname, **pdic)
            elif cmd == 'cooltags':
                log.info(f'Creating cooltag {cooltname} associate to align tag {tname} and args {pdic}')
                if cooltname is None or tname is None:
                    print(f"Cannot create cooltag without the name of a cool and align tags provided via --tag and --cooltag")
                    return
                out = self.cm.create_cooltags(name=cooltname, aligntag=tname, **pdic)
            elif cmd == 'opticallines':
                log.info(f'Creating opticallines {optline} and args {pdic}')
                if optline is None:
                    print(f"Cannot create opticalline without the name provided via --olname")
                    return
                out = self.cm.create_opticallines(name=optline, **pdic)
            elif cmd == 'iovs':
                since = None
                until = None
                if 'since' in args:
                    since = args['since']
                if 'until' in args:
                    until = args['until']
                log.info(f'Creating new iov for tag {tname} time {since} and {until} and args {pdic}')
                out = self.cm.create_iovs(name=tname, since=since, until=until, **pdic)
            elif cmd == 'corrections':
                dataA = None
                dataB = None
                dataFS = None
                dataFP = None
                if 'alines' in corrdic and corrdic['alines'] is not None:
                    dataA = read_AB_lines(corrdic['alines'])
                if 'blines' in corrdic and corrdic['blines'] is not None:
                    dataB = read_AB_lines(corrdic['blines'])
                if 'fitsteps' in corrdic and corrdic['fitsteps'] is not None:
                    dataFS = read_fitsteps(corrdic['fitsteps'])
                if 'fitpulls' in corrdic and corrdic['fitpulls'] is not None:
                    dataFP = read_fitpulls(corrdic['fitpulls'])
                if dataA is not None:
                    corr_ab = merge_corrections(dataA,dataB)
                    log.info(f'Creating new set of corrections for iov {iovid} and corrections list of size {len(corr_ab)}')
                    out = self.cm.create_corrections(iovid=iovid, resources=corr_ab)
                if dataFS is not None:
                    log.info(f'Creating new set of fitsteps for iov {iovid} and fitsteps list of size {len(dataFS)}')
                    out = self.cm.create_fitsteps(iovid=iovid, resources=dataFS)
                if dataFP is not None:
                    log.info(
                        f'Creating new set of fitpulls for iov {iovid} and fitpulls list of size {len(dataFP)}')
                    out = self.cm.create_fitpulls(iovid=iovid, resources=dataFP)
            elif cmd == 'imconfigs':
                log.info(f'Creating new IntervalMaker config and tag with {tname} and args {pdic}')
                out = self.cm.create_imconf_tags(tagname=tname, **pdic)
            else:
                log.error(f'Command {cmd} is not recognized in this context')

        else:
            log.warning('Cannot create object without arguments')
        print(f'Response is : {out}')

    def do_convert(self, line):
        """convert date
        Convert a date to UTC unix time."""
        dt = datetime.fromisoformat(line)
        log.info('create time from string %s %s' % (line, dt.timestamp()))
        since = int(dt.timestamp() * 1000)
        print(f'date {line} = {since}')

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

    def get_corrections_dict(self, corrs):
        pdic={}
        pararr = corrs.split(',')
        for par in pararr:
            kv = par.split('=')
            pdic[kv[0]] = kv[1]
        return pdic

    def check_tagname(self, tag):
        if str(tag).startswith('EC_A'):
            return ('ENDCAP', 'SIDEA')
        elif str(tag).startswith('EC_C'):
            return ('ENDCAP', 'SIDEC')
        elif str(tag).startswith('BA_'):
            return ('BARREL', 'NONE')
        elif str(tag).startswith('TGC_A'):
            return ('TGC', 'SIDEA')
        elif str(tag).startswith('TGC_C'):
            return ('TGC', 'SIDEC')
        log.error('This tag name %s is not recognized as standard', tag)
        return None

if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Align browser.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create', 'dump'], default='ls')
    parser.add_argument('--type', choices=['tags', 'iovs', 'corrections', 'fitsteps',
                                           'cooltags', 'opticallines', 'imtags', 'imiovs', 'imconfigs'], default='tags')
    parser.add_argument('--host', default='localhost',
                        help='Host of the Align service (default: aiatlas061.cern.ch)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='8080',
                        help='Port of the AlignMon service (default: 8080)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-m", "--mode", help="the mode to request intervalmaker iovs: [history, execute]", default="history")
    parser.add_argument("-t", "--tag", help="the tag name")
    parser.add_argument("-c", "--cooltag", help="the cool tag name")
    parser.add_argument("-i","--iovid", default=None, help="the IovID.")
    parser.add_argument("--olname", help="the opticalline name")
    parser.add_argument("--params", help="the string containing k=v pairs for tags or global tags creation")
    parser.add_argument("--cut", help="additional selection parameters. e.g. since>1000,until<2000")
    parser.add_argument("--inpfile", help="the input file to upload")
    parser.add_argument("--since", help="the since time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--until", help="the until time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--corrections", help="the list of files to upload: alines=alines.txt,blines=blines.txt,"
                                              "fitsteps=fs.txt,pulls=pulls.txt")
    parser.add_argument("--side", default='NONE', help="the Endcap Side [A|C].")
    parser.add_argument("--page", default="0", help="the page number.")
    parser.add_argument("--size", default="100", help="the page size.")
    parser.add_argument("--sort", default="none", help="the sort parameter (depend on the selection).")
    parser.add_argument("-f", "--fields", default='none',
                        help="the list of fields to show, separated with a comma. Use -f help to get the available fields.")
    parser.add_argument("-H", "--header", default="BLOB", help="set header request for payload: BLOB, JSON, ...")

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = AlignCli()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_parser(parser)
    argsdic = vars(args)
    if args.fields:
        if args.fields == 'help':
            if args.cmd == 'tags':
                print(f'Fields for {args.cmd} are {tagfieldsdictypes.keys()}')
            else:
                print('not available for this command')
            sys.exit()
    if args.params:
        if args.params == 'help':
            print_help(args.type)
            sys.exit()
    if args.corrections:
        if args.corrections == 'help':
            print_corrections()
            sys.exit()
    if args.cmd in ['ls']:
        log.info(f'Launch ls command on {args.type}')
        ui.do_ls(argsdic)
    elif args.cmd in ['create']:
        log.info(f'Launch create command on {args.type}')
        ui.do_create(argsdic)
    elif args.cmd in ['dump']:
        log.info(f'Launch dump command on {args.type}')
        ui.do_dump(argsdic)
    else:
        log.info(f'Cannot launch command {args.cmd}')
