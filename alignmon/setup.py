"""
 distutils file to install the python Alignmon and Http communication wrapper httpio

 The resulting module is called natsio and can be used in python:
       * from alignmon.io import AlignDbIo

 Henri Louvin - henri.louvin@cea.fr
 Andrea Formica - andrea.formica@cern.ch
"""
from setuptools import setup, find_namespace_packages

setup(name='alignmon',
      version='1.0',
      author="Henri Louvin, Andrea Formica",
      description="""Python module wrapping HttpIo for alignment monitoring use """,
      python_requires=">=3.5",
      namespace_packages=['alignmon'],
      packages=find_namespace_packages(include=['alignmon.*']),
      install_requires=['asyncio',
                        'requests',
                        'aiohttp'],
     )
