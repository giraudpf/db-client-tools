from coolutils import CoolMgr
from aligncli import AlignCli
from datetime import datetime

ui = AlignCli()
ui.set_host('http://aiatlas092.cern.ch:9090/api')
ui.do_connect()
args = { 'tag': 'BA_0800_RELATIVE', 'type': 'iovs', 'sort' : 'iovId:ASC', 'page': 0, 'size': 1000 }
iovset = ui.do_ls(args)
print(f'Found list of iovs: {iovset}')

coolui = CoolMgr('sqlite://;schema=./align2017.db;dbname=CONDBR2')
coolui.open('update')

iovlist = iovset['resources']

for iov in iovlist:
    iovid = iov['iovId']
    tag = 'MuonAlignMDTBarrelAlign-RUN2-AF-TEST-01'
    node = '/MUONALIGN/MDT/BARREL'
    args = {'type': 'iovs', 'iovid': iovid }
    iovdump_filename = ui.do_dump(args)
    print(f'IOV {iovid} has been dumped in {iovdump_filename}')
    since = iov['sinceT']
    todo = iov['coolStatus']
    if 'TODO' == todo:
        if isinstance(since, str):
            dt = since
            li = dt.rsplit(':', 1)
            dt = ''.join(li)
            pdt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S%z')
            print(f'Use since time {pdt}')
            since = int(pdt.strftime('%s')) * 1000000000
        pdic = { 'tech': '1', 'file': str(iovid), 'data': f'@{iovdump_filename}'}
        user_rec = coolui.parseParams(**pdic)
        print(f'create iov record {user_rec} using since {since} into {node} / {tag}')
        coolui.storeObject(folder=node, data=user_rec, since=since, channel=1, tag=tag)
        print(f'done {iovid}')

coolui.close()
