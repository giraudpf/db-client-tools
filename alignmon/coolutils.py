#!/usr/bin/env python

'''
Created on Sep 14, 2020

@author: formica
'''

import argparse
import logging
import os
import sys
from PyCool import cool
import distutils
from datetime import datetime

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, \
                    format='%(asctime)s %(levelname)s [%(name)s] %(message)s')

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

sys.path.append(os.path.join(sys.path[0], '..'))


class CoolMgr(object):
    """Simple COOL command processor example."""

    def __init__(self, db_url=None, db_pass=None):
        if db_url is None:
            db_url = f'oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_MUONALIGN;dbname=CONDBR2;user=ATLAS_COOL_READER;password={dbpass}'
        else:
            if db_url.startswith('oracle'):
                db_url = f'{db_url};password={db_pass}'
        cm = None
        self.db_url = db_url
        self.db_svc = cool.DatabaseSvcFactory.databaseService()
        self.dbconn = None
        self.rec_spec = None
        self.recspectypes = {
        'Bool': cool.StorageType.Bool,
        'UChar': cool.StorageType.UChar,
        'Int16': cool.StorageType.Int16,
        'UInt16': cool.StorageType.UInt16,
        'Int32': cool.StorageType.Int32,
        'UInt32': cool.StorageType.UInt32,
        'UInt63': cool.StorageType.UInt63,
        'Int64': cool.StorageType.Int64,
        'Float': cool.StorageType.Float,
        'Double': cool.StorageType.Double,
        'String255': cool.StorageType.String255,
        'String4k': cool.StorageType.String4k,
        'String64k': cool.StorageType.String64k,
        'String16M': cool.StorageType.String16M,
        'Blob64k': cool.StorageType.Blob64k,
        'Blob16M': cool.StorageType.Blob16M
        }

    def open(self, flag):
        try:
            url = self.db_url
            if flag == 'create':
                log.info(f'Create db using: {url}')
                self.dbconn = self.db_svc.createDatabase(url)
            elif flag == 'open':
                log.info(f'Open db using: {url}')
                self.dbconn = self.db_svc.openDatabase(url)
            elif flag == 'update':
                log.info(f'Open db for update using: {url}')
                self.dbconn = self.db_svc.openDatabase(url, False)

        except Exception as e:
            log.info(f'Problem creating database: {e}')

    def listAllNodes(self):
        try:
            nodelist = self.dbconn.listAllNodes()
            for node in nodelist:
                log.info(f'Found node {node} in source db')
        except Exception as e:
            print(e)

    def close(self):
        try:
            self.dbconn.closeDatabase()
        except Exception as e:
            print(e)

    def getFolder(self, folder):
        cool_folder = None
        try:
            cool_folder = self.dbconn.getFolder(folder)
        except Exception as e:
            cool_folder = self.dbconn.getFolderSet(folder)
            print(f'tried to get a folderset for {folder}')
        return cool_folder

    def getObjects(self, folder, since, until):
        cool_folder = self.getFolder(folder)
        ## cool.ChannelSelection(0)
        objs = cool_folder.browseObjects(since, until)
        return objs

    def createFolderAsCopy(self, source_folder=None, dest_folder=None):
        ## folder
        spec = source_folder.folderSpecification()
        desc = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'
        mode = cool.PayloadMode.SEPARATEPAYLOAD
        new_folder = self.dbconn.createFolder(dest_folder, spec, desc, cool.FolderVersioning.MULTI_VERSION, True)
        cool_channels = source_folder.listChannelsWithNames()
        for chan in cool_channels:
            log.info(f'Found channel {chan[0]} {chan[1]}')
            new_folder.createChannel(chan[0], chan[1], "");
        return new_folder

    def createFolder(self, folder_name=None, rec_spec=None, folder_desc=None, mode=cool.PayloadMode.SEPARATEPAYLOAD):
        ## folder
        spec = cool.FolderSpecification(mode, rec_spec)
        desc = folder_desc
        new_folder = self.dbconn.createFolder(folder_name, spec, desc, mode, True)
        return new_folder

    def storeObject(self, folder=None, data=None, since=cool.ValidityKeyMin, until=cool.ValidityKeyMax, channel=1,
                    tag=None):
        cool_folder = self.getFolder(folder)
        recspec = cool_folder.payloadSpecification()
        cool_data = cool.Record(recspec)
        for k, v in data.items():
            field_spec = recspec[k]
            type = self.getType(field_spec.storageType())
            print(f'Adding field {k} with value {v} and type {type}')
            val = v
            if 'Bool' == type:
                val = distutils.util.strtobool(v)
            if type.startswith('UI') or type.startswith('In'):
                val = int(v)
            if type.startswith('F') or type.startswith('D'):
                val = float(v)
            if type.startswith('Str'):
                check = isinstance(v, str)
                print(f'convert {v} into string if is not ({check})')
                if not isinstance(v, str):
                    val = v.decode()
            print(f'store value {val}')
            cool_data[k] = val
        cool_folder.storeObject(since, until, cool_data, channel, tag, True)

    def listTags(self, folder):
        cool_folder = None
        taglist = []
        cool_folder = self.getFolder(folder)
        taglist = cool_folder.listTags()
        return taglist

    def parseParams(self, **kwargs):
        import re
        user_record = {}
        for key, val in kwargs.items():
            user_record[key] = val
            if val.startswith('@'): # use val as a filename
                file_name = re.sub('[@]', '', val)
                with open(file_name, 'rb') as fin:
                    data = fin.read()
                    user_record[key] = data
        return user_record

    def getType(self, storagetype=None):
        for k, v in self.recspectypes.items():
            if v == storagetype:
                return k
        return None

    def buildRecSpec(self, params={}):
        self.rec_spec =  cool.RecordSpecification()
        for k,v in params.items():
            print(f'extend record using {k} {v}')
            if v not in self.recspectypes.keys():
                print(f'Error in creating folder spec: type {v} does not exists')
                sys.exit(-1)
            self.rec_spec.extend(k, self.recspectypes[v])
        return self.rec_spec

if __name__ == '__main__':
    # Parse arguments
    desc = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'

    parser = argparse.ArgumentParser(description='Cool browser.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create'], default='ls')
    parser.add_argument('--type', choices=['nodes', 'tags', 'iovs'],
                        default='schemas')
    parser.add_argument('--cooldb',
                        default='oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_MUONALIGN;dbname=CONDBR2;user=ATLAS_COOL_READER',
                        help='COOL connection')
    parser.add_argument('--password', default=None,
                        help='password for the COOL DB')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-n", "--node", help="the node name")
    parser.add_argument("-t", "--tag", help="the tag name")
    parser.add_argument("-g", "--globaltag", help="the global tag name")
    parser.add_argument("--params", help="the string containing k=v pairs for tags or global tags creation")
    parser.add_argument("--desc", default=desc, help="the string containing folder description")
    parser.add_argument("--iovtype", default='cooltime', help="the iov type: cooltime, run-lumi, iso")
    parser.add_argument("--runmin", help="the start run number")
    parser.add_argument("--runmax", default="999999", help="the end run number")
    parser.add_argument("--chan", help="the channel id")
    parser.add_argument("--since", help="the since time")
    parser.add_argument("--until", default="INF", help="the until time")
    parser.add_argument("--mode", default="open", help="mode to open database: [create | open | update]")
    parser.add_argument("--channel", default='%', help="the channel id or a name pattern")
    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    ui = CoolMgr(db_url=args.cooldb, db_pass=args.password)
    log.info('Start application')
    ui.open(args.mode)
    pdic = {}
    if args.until == "INF":
        args.until = cool.ValidityKeyMax

    if args.iovtype == 'iso':
        if isinstance(args.since, str):
            dt = args.since
            li = dt.rsplit(':', 1)
            dt = ''.join(li)
            pdt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S%z')
            print(f'Use since time {pdt}')
            args.since = int(pdt.strftime('%s')) * 1000000000

        if isinstance(args.until, str):
            dt = args.until
            li = dt.rsplit(':', 1)
            dt = ''.join(li)
            pdt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S%z')
            print(f'Use until time {pdt}')
            args.until = int(pdt.strftime('%s')) * 1000000000

    if args.params is not None:
        pararr = args.params.split(',')
        for par in pararr:
            kv = par.split('=')
            pdic[kv[0]] = kv[1]

    if 'nodes' in args.type and 'ls' in args.cmd:
        node = args.node
        cool_node = ui.getFolder(node)
        log.info(f'Found cool node {cool_node}')

    elif 'tags' in args.type and 'ls' in args.cmd:
        node = args.node
        taglist = ui.listTags(node)
        for tag in taglist:
            log.info(f'Found tag {tag}')

    elif 'iovs' in args.type and 'create' in args.cmd:
        user_rec = ui.parseParams(**pdic)
        print(f'create iov record {user_rec} using since {args.since} into {args.node} / {args.tag}')
        ui.storeObject(folder=args.node, data=user_rec, since=int(args.since), until=int(args.until), channel=int(args.chan), tag=args.tag)

    elif 'nodes' in args.type and 'create' in args.cmd:
        user_spec = ui.parseParams(**pdic)
        print(f'Create node {args.node} using {user_spec}')
        rec_spec = ui.buildRecSpec(user_spec)
        user_folder = ui.createFolder(folder_name=args.node, rec_spec=rec_spec, folder_desc=args.desc)

    else:
        log.warning(f'Cannot recognize command - type {args.cmd} {args.type}')
    ### Close database
    ui.close()

#
# Example of params for node creation
# ## prepare cool folder record
#                 self.rec = cool.RecordSpecification()
#                 self.rec.extend("data"   , cool.StorageType.Blob16M)
#                 self.rec.extend("mapping", cool.StorageType.Blob64k)
#                 self.rec.extend("test", cool.StorageType.String4k)
# Full available list : https://gitlab.cern.ch/lcgcool/cool/-/blob/master/src/CoolKernel/CoolKernel/StorageType.h