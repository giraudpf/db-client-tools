'''
Created on Nov 24, 2017

@author: formica
'''

import sys, os
import logging
import atexit

print('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__, str(__package__)))

pullsfieldsdicheader = {
    'olname': {'key': '{olname:25s}', 'val': 'Name', 'field': '{olname:25s}', 'type': 'str'},
    'iovid': {'key': '{iovid:20s}', 'val': 'Iov', 'field': '{iovid:10d}', 'type': 'long'},
    'xPull': {'key': '{xPull:10s}', 'val': 'X Pull', 'field': '{xPull:10f}', 'type': 'float'},
    'yPull': {'key': '{yPull:10s}', 'val': 'Y Pull', 'field': '{yPull:10f}', 'type': 'float'},
    'zPull': {'key': '{zPull:10s}', 'val': 'Z Pull', 'field': '{zPull:10f}', 'type': 'float'},
}
sagittasfieldsdicheader = {
    'sagId': {'key': '{sagId:10s}', 'val': 'Id', 'field': '{sagId:25s}', 'type': 'long'},
    'xtrack': {'key': '{xtrack:10s}', 'val': 'X track', 'field': '{xtrack:10f}', 'type': 'float'},
    'ytrack': {'key': '{ytrack:10s}', 'val': 'Y track', 'field': '{ytrack:10f}', 'type': 'float'},
    'ztrack': {'key': '{ztrack:10s}', 'val': 'Z track', 'field': '{ztrack:10f}', 'type': 'float'},
    'uxtrack': {'key': '{uxtrack:10s}', 'val': 'UX track', 'field': '{uxtrack:10f}', 'type': 'float'},
    'uytrack': {'key': '{uytrack:10s}', 'val': 'UY track', 'field': '{uytrack:10f}', 'type': 'float'},
    'uztrack': {'key': '{uztrack:10s}', 'val': 'UZ track', 'field': '{uztrack:10f}', 'type': 'float'},
    'bm': {'key': '{bm:10s}', 'val': 'BM', 'field': '{bm:10s}', 'type': 'str'},
    'bi': {'key': '{bi:10s}', 'val': 'BI', 'field': '{bi:10s}', 'type': 'str'},
    'bo': {'key': '{bo:10s}', 'val': 'BO', 'field': '{bo:10s}', 'type': 'str'},
}
stepsfieldsdicheader = {
    'fitId': {'key': '{fitId:10s}', 'val': 'Id', 'field': '{fitId:10d}', 'type': 'long'},
    'chiSquareOvDof': {'key': '{chiSquareOvDof:10s}', 'val': 'Chi2/DoF', 'field': '{chiSquareOvDof:10f}', 'type': 'float'},
    'chiSquare': {'key': '{chiSquare:10s}', 'val': 'Chi2', 'field': '{chiSquare:10f}', 'type': 'float'},
    'dof': {'key': '{dof:7s}', 'val': 'DoF', 'field': '{dof:7d}', 'type': 'int'},
    'prob': {'key': '{prob:8s}', 'val': 'Prob', 'field': '{prob:8f}', 'type': 'float'},
    'sequenceNumber': {'key': '{sequenceNumber:5s}', 'val': 'Seq', 'field': '{sequenceNumber:5d}', 'type': 'int'},
    'fitDescription': {'key': '{fitDescription:30s}', 'val': 'Description', 'field': '{fitDescription:30s}', 'type': 'str'},
    'nIterations': {'key': '{nIterations:6s}', 'val': 'NIter', 'field': '{nIterations:6d}', 'type': 'int'},
    'exitCode': {'key': '{exitCode:10s}', 'val': 'ExCode', 'field': '{exitCode:5d}', 'type': 'int'},
    'branch': {'key': '{branch:10s}', 'val': 'Branch', 'field': '{branch:10s}', 'type': 'str'},
    'revision': {'key': '{revision:10s}', 'val': 'Revision', 'field': '{revision:10s}', 'type': 'str'},
}
optlinefieldsdicheader = {
    'olname': {'key': '{olname:25s}', 'val': 'Name', 'field': '{olname:25s}', 'type': 'str'},
    'oltype': {'key': '{oltype:20s}', 'val': 'Type', 'field': '{oltype:20s}', 'type': 'str'},
    'olstatus': {'key': '{olstatus:20s}', 'val': 'Status', 'field': '{olstatus:20s}', 'type': 'str'},
}
corrfieldsdicheader = {
    'corrId': {'key': '{corrId:10s}', 'val' : 'Id', 'field': '{corrId:10d}', 'type': 'int'},
    'corrDescription': {'key': '{corrDescription:4s}','val' : 'Desc', 'field': '{corrDescription:4s}', 'type': 'str'},
    'typ': {'key': '{typ:4s}','val' : 'Typ', 'field': '{typ:4s}', 'type': 'str'},
    'jff': {'key': '{jff:3s}','val' : 'Jff', 'field': '{jff:3d}', 'type': 'int'},
    'jzz': {'key': '{jzz:3s}','val' : 'Jzz', 'field': '{jzz:3d}', 'type': 'int'},
    'job': {'key': '{job:3s}','val' : 'Job', 'field': '{job:3d}', 'type': 'int'},
    'svalue': {'key': '{svalue:7s}','val' : 'S', 'field': '{svalue:7f}', 'type': 'float'},
    'zvalue': {'key': '{zvalue:7s}','val' : 'Z', 'field': '{zvalue:7f}', 'type': 'float'},
    'tvalue': {'key': '{tvalue:7s}','val' : 'T', 'field': '{tvalue:7f}', 'type': 'float'},
    'tsv': {'key': '{tsv:7s}','val' : 'ts', 'field': '{tsv:7f}', 'type': 'float'},
    'tzv': {'key': '{tzv:7s}','val' : 'tz', 'field': '{tzv:7f}', 'type': 'float'},
    'ttv': {'key': '{ttv:7s}','val' : 'tt', 'field': '{ttv:7f}', 'type': 'float'},
    'bz': {'key': '{bz:7s}','val' : 'BZ', 'field': '{bz:7f}', 'type': 'float'},
    'bp': {'key': '{bp:7s}','val' : 'BP', 'field': '{bp:7f}', 'type': 'float'},
    'bn': {'key': '{bn:7s}','val' : 'BN', 'field': '{bn:7f}', 'type': 'float'},
    'sp': {'key': '{sp:7s}','val' : 'SP', 'field': '{sp:7f}', 'type': 'float'},
    'sn': {'key': '{sn:7s}','val' : 'SN', 'field': '{sn:7f}', 'type': 'float'},
    'tw': {'key': '{tw:7s}','val' : 'TW', 'field': '{tw:7f}', 'type': 'float'},
    'pg': {'key': '{pg:7s}','val' : 'PG', 'field': '{pg:7f}', 'type': 'float'},
    'tr': {'key': '{tr:7s}','val' : 'TR', 'field': '{tr:7f}', 'type': 'float'},
    'eg': {'key': '{eg:7s}','val' : 'EG', 'field': '{eg:7f}', 'type': 'float'},
    'ep': {'key': '{ep:7s}','val' : 'EP', 'field': '{ep:7f}', 'type': 'float'},
    'en': {'key': '{en:7s}','val' : 'EN', 'field': '{en:7f}', 'type': 'float'},
    'hwElement': {'key': '{hwElement:10s}','val' : 'Element', 'field': '{hwElement:10s}', 'type': 'str'},
}

iovfieldsdicheader = {
    'iovId': {'key': '{iovId:10s}', 'val': 'Id', 'field': '{iovId:10d}', 'type': 'int'},
    'sinceT': {'key': '{sinceT:30s}', 'val': 'Since', 'field': '{sinceT:30s}', 'type': 'str'},
    'tillT': {'key': '{tillT:30s}', 'val': 'Until', 'field': '{tillT:30s}', 'type': 'str'},
    'fitStatus': {'key': '{fitStatus:3s}', 'val': 'Fit', 'field': '{fitStatus:3d}', 'type': 'int'},
    'coolStatus': {'key': '{coolStatus:20s}', 'val': 'Cool', 'field': '{coolStatus:20s}', 'type': 'str'},
    'iovComment': {'key': '{iovComment:20s}', 'val': 'Comment', 'field': '{iovComment:20s}', 'type': 'str'},
}

imiovfieldsdicheader = {
    'iovId': {'key': '{iovId:10s}', 'val': 'Id', 'field': '{iovId:10d}', 'type': 'int'},
    'since': {'key': '{since:30s}', 'val': 'Since', 'field': '{since:30s}', 'type': 'str'},
    'till': {'key': '{till:30s}', 'val': 'Until', 'field': '{till:30s}', 'type': 'str'},
    'fitStatus': {'key': '{fitStatus:3s}', 'val': 'Fit', 'field': '{fitStatus:3d}', 'type': 'int'},
    'coolStatus': {'key': '{coolStatus:20s}', 'val': 'Cool', 'field': '{coolStatus:20s}', 'type': 'str'},
    'iovComment': {'key': '{iovComment:20s}', 'val': 'Comment', 'field': '{iovComment:20s}', 'type': 'str'},
}

tagfieldsdicheader = {
    'tagId': {'key': '{tagId:10s}', 'val': 'TagId', 'field': '{tagId:10d}', 'type': 'long'},
    'iovTag': {'key': '{iovTag:25s}', 'val': 'Name', 'field': '{iovTag:25s}', 'type': 'str'},
    'tagType': {'key': '{tagType:10s}', 'val': 'Type', 'field': '{tagType:10s}', 'type': 'str'},
    'tagInsertion': {'key': '{tagInsertion:30s}', 'val': 'Insertion', 'field': '{tagInsertion:30s}', 'type': 'str'},
    'tagDescription': {'key': '{tagDescription:50s}', 'val': 'Description', 'field': '{tagDescription:50s}', 'type': 'str'},
    'tagAuthor': {'key': '{tagAuthor:15s}', 'val': 'Author', 'field': '{tagAuthor:15s}', 'type': 'str'},
    'prevTag': {'key': '{prevTag:10s}', 'val': 'PrevId', 'field': '{prevTag:10d}', 'type': 'long'},
}
imtagfieldsdicheader = {
    'tagId': {'key': '{tagId:10s}', 'val': 'TagId', 'field': '{tagId:10d}', 'type': 'long'},
    'tagName': {'key': '{tagName:25s}', 'val': 'Name', 'field': '{tagName:25s}', 'type': 'str'},
    'tagDescription': {'key': '{tagDescription:50s}', 'val': 'Description', 'field': '{tagDescription:50s}', 'type': 'str'},
    'tagInsertion': {'key': '{tagInsertion:30s}', 'val': 'Insertion', 'field': '{tagInsertion:30s}', 'type': 'str'},
}
imiovfieldsdicheader = {
    'id': {'key': '{id:10s}', 'val': 'Id', 'field': '{id:10d}', 'type': 'int'},
    'since': {'key': '{since:30s}', 'val': 'Since', 'field': '{since:30s}', 'type': 'str'},
    'till': {'key': '{till:30s}', 'val': 'Until', 'field': '{till:30s}', 'type': 'str'},
    'isFloating': {'key': '{isFloating:10s}', 'val': 'Floating', 'field': '{isFloating:10s}', 'type': 'bool'},
    'isStable': {'key': '{isStable:10s}', 'val': 'Stable', 'field': '{isStable:10s}', 'type': 'bool'},
}

imconfigfieldsdicheader = {
    'tagName': {'key': '{tagName:25s}', 'val': 'Name', 'field': '{tagName:25s}', 'type': 'str'},
    'tagDescription': {'key': '{tagDescription:50s}', 'val': 'Description', 'field': '{tagDescription:50s}', 'type': 'str'},
    'saveToDB': {'key': '{saveToDB:6s}', 'val': 'DB store', 'field': '{saveToDB:6s}', 'type': 'bool'},
    'periodMinutes': {'key': '{periodMinutes:12s}', 'val': 'Periodicity', 'field': '{periodMinutes:12d}', 'type': 'int'},
    'iovRange': {'key': '{iovRange:10s}', 'val': 'Range', 'field': '{iovRange:10d}',
                      'type': 'int'},
    'iovMinLength': {'key': '{iovMinLength:10s}', 'val': 'MinLen', 'field': '{iovMinLength:10d}',
                 'type': 'int'},
    'magCurrentCut': {'key': '{magCurrentCut:10s}', 'val': 'MagCurrCut', 'field': '{magCurrentCut:10.2f}',
                     'type': 'float'},
}

cooltagfieldsdicheader = {
    'alignTag': {'key': '{alignTag:25s}', 'val': 'AlignTag', 'field': '{alignTag:25s}', 'type': 'str'},
    'coolTag': {'key': '{coolTag:25s}', 'val': 'CoolTag', 'field': '{coolTag:25s}', 'type': 'str'},
    'tagType': {'key': '{tagType:10s}', 'val': 'Type', 'field': '{tagType:10s}', 'type': 'str'},
    'tagComment': {'key': '{tagComment:30s}', 'val': 'Comment', 'field': '{tagComment:30s}', 'type': 'str'},
    'doUploadOnReco': {'key': '{doUploadOnReco:5s}', 'val': 'upload', 'field': '{doUploadOnReco:5s}', 'type': 'str'},
    'cherrypyConfig': {'key': '{cherrypyConfig:25s}', 'val': 'CherryPy', 'field': '{cherrypyConfig:25s}', 'type': 'str'},
    'coollock': {'key': '{coollock:10s}', 'val': 'Lock', 'field': '{coollock:10s}', 'type': 'str'},
}

def server_print(srvdata, format=[]):
    if srvdata is None or 'size' not in srvdata.keys():
        print('Cannot find results to print')
        return
    size = srvdata['size']
    dataarr = []
    print(f'Found data list of size {size}')
    if 'resources' in srvdata:
        print(f'Retrieved {size} lines')
        dataarr = srvdata['resources']
    if (srvdata['format'] == 'AligntagSetDto'):
        dprint(format, tagfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'OpticallinesSetDto'):
        dprint(format, optlinefieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AligniovSetDto'):
        dprint(format, iovfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AligncorrectionsSetDto'):
        dprint(format, corrfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignFitPullsSetDto'):
        dprint(format, pullsfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignFitStepsSetDto'):
        dprint(format, stepsfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignCoolTagSetDto'):
        dprint(format, cooltagfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignSagittasSetDto'):
        dprint(format, sagittasfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'IntervalMakerTagSetDto'):
        dprint(format, imtagfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'IntervalMakerConfigurationSetDto'):
        dprint(format, imconfigfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'IntervalMakerIovSetDto'):
        dprint(format, imiovfieldsdicheader, dataarr)
    else:
        print(srvdata)


def dprint(format, headerdic, cdata):
    if len(format) == 0:
        format = headerdic.keys()
    headerfmtstr = ' '.join([headerdic[k]['key'] for k in format])
    # print(f'The format string is {headerfmtstr}')
    headic = {}
    for k in format:
        headic[k] = headerdic[k]['val']
        # print(f'Using format {headic[k]} for {k}')
    print(headerfmtstr.format(**headic))
    # print('Use format %s' % format)
    fmtstr = ' '.join([headerdic[k]['field'] for k in format])
    # print('Format string %s'%fmtstr)
    for xt in cdata:
        adic = {}
        for k in format:
            if xt[k] is None:
                if headerdic[k]['type'] in ['int', 'long', 'float']:  # True
                    xt[k] = -1
                else:
                    xt[k] = ' - '
            if headerdic[k]['type'] in ['bool']:
                adic[k] = str(xt[k])
            else:
                adic[k] = xt[k]
        # print('Use dictionary %s'%adic)
        print(fmtstr.format(**adic))


def print_help(data_type):
    if (data_type == 'tags'):
        print(f'Parameters for {data_type} are: {tagfieldsdicheader.keys()}')
    elif (data_type == 'opticallines'):
        print(f'Parameters for {data_type} are: {optlinefieldsdicheader.keys()}')
    elif (data_type == 'iovs'):
        print(f'Parameters for {data_type} are: {iovfieldsdicheader.keys()}')
    elif (data_type == 'corrections'):
        print(f'Parameters for {data_type} are: {corrfieldsdicheader.keys()}')
    elif (data_type == 'fitsteps'):
        print(f'Parameters for {data_type} are: {stepsfieldsdicheader.keys()}')
    elif (data_type == 'fitpulls'):
        print(f'Parameters for {data_type} are: {pullsfieldsdicheader.keys()}')
    elif (data_type == 'aligncooltag'):
        print(f'Parameters for {data_type} are: {cooltagfieldsdicheader.keys()}')
    elif (data_type == 'sagittas'):
        print(f'Parameters for {data_type} are: {sagittasfieldsdicheader.keys()}')
    elif (data_type == 'imtags'):
        print(f'Parameters for {data_type} are:  {imtagfieldsdicheader.keys()}')
    elif (data_type == 'imiovs'):
        print(f'Parameters for {data_type} are:  {imiovfieldsdicheader.keys()}')
    else:
        print('Cannot find help for this kind of data_type')


def print_corrections():
    print('List of possible correction files and keys:')
    print('alines=<the alines file name>')
    print('blines=<the blines file name>')
    print('fitsteps=<the fitsteps file name>')
    print('fitpulls=<the fitpulls file name>')
    print('badsens=<the bad sensors file name>')
    print('sagittas=<the sagittas file name>')

def read_fitpulls(filename):
    import re
    headers = ['olname', 'xPull', 'yPull', 'zPull']
    with open(filename) as f:
        header = next(f, None)
        print(f'found headers {header}, but it is ignored for this file, and we use {headers}')
        alldic = []
        for row in f.readlines():
            fields = re.split('\s+', row)
            dictionary = dict(zip(headers, fields))
            for k in dictionary.keys():
                dictionary[k] = convert_fp_tonumber(dictionary[k].strip(), k)
            alldic.append(dictionary)
    #            print(f'{row}')
    f.close()
    return alldic


def read_fitsteps(filename):
    import re
    headers = ['chiSquareOvDof', 'chiSquare', 'dof', 'prob', 'sequenceNumber', 'fitDescription']
    with open(filename) as f:
        header = next(f, None)
        print(f'found headers {header}, but it is ignored for this file, and we use {headers}')
        alldic = []
        for row in f.readlines():
            fields = re.split('\s+\|\s+', row)
            dictionary = dict(zip(headers, fields))
            for k in dictionary.keys():
                dictionary[k] = convert_fs_tonumber(dictionary[k].strip(), k)
            alldic.append(dictionary)
    #            print(f'{row}')
    f.close()
    return alldic


def read_AB_lines(filename):
    headersCommon = ['side', 'typ', 'jff', 'jzz', 'job']
    headersA = ['svalue', 'zvalue', 'tvalue', 'tsv', 'tzv', 'ttv']
    headersB = ['bz', 'bp', 'bn', 'sp', 'sn', 'tw', 'pg', 'tr', 'eg', 'ep', 'en']
    headerTrail = ['hwElement']
    with open(filename) as f:
        header = next(f, None)
        print(f'found headers {header}')
        headfields = header.split()
        alldic = []
        for row in f.readlines():
            fields = row.split()
            dictionary = dict(zip(headfields, fields))
            alldic.append(dictionary)
    #            print(f'{row}')
    f.close()
    return alldic


def merge_corrections(dictarrA, dictarrB):
    newdictarrAB = []
    for al in dictarrA:
        dictAB = {'corrDescription': 'AB'}
        for akeys in al.keys():
            if akeys not in ['corrDescription']:
                dictAB[akeys] = convert_corr_tonumber(al[akeys], akeys)
        bel = get_hwelement(dictarrB, al['hwElement'])
        # suppose that only A lines were provided, we should still upload them
        if bel is None:
            bel = {'bz': 0, 'bp': 0, 'bn': 0, 'sp': 0, 'sn': 0, 'tw': 0, 'pg': 0, 'tr': 0, 'eg': 0, 'ep': 0, 'en': 0}
        for bkeys in bel.keys():
            if bkeys not in ['corrDescription', 'typ', 'jff', 'jzz', 'job', 'hwElement']:
                dictAB[bkeys] = convert_corr_tonumber(bel[bkeys], bkeys)
        newdictarrAB.append(dictAB)
    return newdictarrAB


def get_hwelement(dictarr, element):
    for el in dictarr:
        if el['hwElement'] == element:
            return el
    return None

def convert_fs_tonumber(val, element):
    if element not in ['fitDescription']:
        if element not in ['sequenceNumber', 'dof']:
            return float(val)
        return int(val)
    return val

def convert_fp_tonumber(val, element):
    if element not in ['olname']:
        if element not in ['iovid']:
            return float(val)
        return int(val)
    return val

def convert_corr_tonumber(val, element):
    if element not in ['corrDescription', 'typ', 'hwElement']:
        if element not in ['jff', 'jzz', 'job']:
            return float(val)
        return int(val)
    return val
