# Alignmon client documentation
This python client is an example to test Align-Mon server API. 

## Requirements
Need python >=3.5

## Installation
Can be installed via pip. 
`pip install . `
or 
`pip install . --user`
for local user installation.

## Usage examples
We have a command line client and a Console which should provide the same functionalities.
### Command line
```
python aligncli.py -h
```
### Console
Launch the command line after the library has been installed.
```
python CliAlignConsole.py
```

```
(py36-venv) $ python CliAlignConsole.py -h
usage: CliAlignConsole.py [-h] [--host HOST] [--api API] [--port PORT]
                          [--socks] [--ssl]

Alignmon browser.
(py37) $ python CliAlignConsole.py
INFO:__main__: The host is set to http://localhost:8080/api
INFO:aligncli: Connected to http://localhost:8080/api
INFO:__main__: Connected to http://localhost:8080/api
INFO:__main__: Start application
(MuonAlign):
....
(MuonAlign): help ls
ls <datatype> [-t tag_name] [--olname opticalline_name] [other options: --size, --page, --sort, --format]
         Search for data collection of different kinds: opticallines, tags.
         datatype: opticallines, tags.
         Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)

(MuonAlign): ls --type tags -t BA_AF*
INFO:aligncli: Searching for data of type tags
INFO:aligncli: Search tags using BA_AF*
Found data list of size 9
Retrieved 9 lines
TagId      Name                      Type       Insertion                      Description                                        Author          PrevId
      1122 BA_AF_TEST                TESTS      2009-11-13 14:00:47.485+00:00  test                                               FORMICA                 -1
      1922 BA_AF_TESTPY_01           test       2008-12-08 16:43:34.685+00:00  an align tag                                       andrea                  -1
....
```