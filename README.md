# DB CLIENT TOOLS
This project contains a set of tools that can be used as an example to extract data from databases.
Some of these tools require external libraries. Follow the instructions in each sub directory for more detailed explanation. 
This is a work in progress and you may find out several things not working.

## Installation of venv (and some hints on lxplus)
On lxplus you should enable the 3.6 python version using:
```
scl enable rh-python36 zsh
```
Then create a virtual environment for the installation of the clients:
```
python -m venv ./py36-venv
source ./py36-venv/bin/activate
python -m pip install --upgrade pip setuptools wheel
```
To install a specific package (`coolr` or `crest`) go into the directory and simply do:
```
pip install .
```
To deactivate the vertual env you should do:
```
deactivate
```
### Next connections
Next time you just need to activate the virtual environment. 
```
source <thepathtoyourenv>/py36-venv/bin/activate
```
Re-installation via 
```
pip install .
```
are needed only if the files `crestdbio.py` or `httpio.py` have been changed.

## coolr
The coolr directory contains simple python clients for COOLR services. It requires the usage of a COOLR server deployed somewhere.
This client allow to browse COOL data (via the COOLR server) using simple REST api and can be installed in any system with python >= 3.6 .
To install only the low level client package you can type:
```
pip3 install "git+https://git@gitlab.cern.ch/formica/db-client-tools.git#egg=coolr&subdirectory=coolr"
```
For further informations please read the [README file](coolr/README.md)
## crest
The crest directory contains a simple python client for CREST services. It requires a CREST server installation or the usage of a known server.
This client allow to browse CREST data (via the CREST server) using simple REST api and can be installed in any system with python >= 3.6 .

## dcs data extraction
Create a pandas dataframe from an SQL query. It requires pandas to be installed. We are not going
to provide full documentation here, the code is more a work in progress to be considered as an example.
